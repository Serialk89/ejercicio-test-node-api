const SaveInfoDTO = require("./save-info-dto");
const { BaseDTO, fields } = require('dtox');
describe("SaveInfoDTO DTO", () => {
       
  let saveInfoDTO = null;

  beforeEach( () => {    
                  
  });

  afterEach( () => {
    saveInfoDTO = null;
  });
     
  it("should be an instance of saveInfoDTO ", () => {    
    saveInfoDTO = new SaveInfoDTO({id:12,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
    expect(saveInfoDTO instanceof SaveInfoDTO).toBe(true);    
  });

  it("should be an instance of BaseDTO ", () => {    
    saveInfoDTO = new SaveInfoDTO({id:12,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
    expect(saveInfoDTO instanceof BaseDTO).toBe(true);    
  });

  it("should be 6 attributes (1  internal extra)", () => {                      
    saveInfoDTO = new SaveInfoDTO({id:12,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
    let count = 0;
    for(value in saveInfoDTO) {
      count++
    }
    expect(count).toBe(6);        
  });

  describe("attribute id", () => {

    it("should throw an excption", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:"sasa",name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");               
        expect(err.message).toBe("Property of type number required");               
      }  
    });

    it("should return the correct value", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:1221,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
        expect(saveInfoDTO.id).toBe(1221);               
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }  
    });     

  });  

  describe("attribute name", () => {

    it("should throw an excption", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:111,name:{}, lastName:"lastname", email:"email", test:"11.111.111-1"});
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");               
        expect(err.message).toBe("Property of type string required");               
      }  
    });

    it("should return the correct value", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:1221,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
        expect(saveInfoDTO.name).toBe("name");               
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }  
    });     

  });

  describe("attribute lastName", () => {

    it("should throw an excption", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:111,name:"name", lastName:1221, email:"email", test:"11.111.111-1"});        
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");               
        expect(err.message).toBe("Property of type string required");               
      }  
    });

    it("should return the correct value", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:1221,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
        expect(saveInfoDTO.lastName).toBe("lastname");               
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }  
    });     

  });  

  describe("attribute email", () => {

    it("should throw an excption", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:111,name:"name", lastName:"lastname", email:3232, test:"11.111.111-1"});        
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");               
        expect(err.message).toBe("Property of type string required");               
      }  
    });

    it("should return the correct value", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:1221,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
        expect(saveInfoDTO.email).toBe("email");               
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }  
    });     

  });

  describe("attribute test", () => {

    it("should throw an excption", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:111,name:"name", lastName:"lastname", email:"email", test:"11.1w11.111-1"});        
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("Error");               
        expect(err.message).toBe("error with params");               
      }  
    });

    it("should return the correct value", () => {
      try{                                   
        saveInfoDTO = new SaveInfoDTO({id:1221,name:"name", lastName:"lastname", email:"email", test:"11.111.111-1"});
        expect(saveInfoDTO.test).toBe("11.111.111-1");               
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }  
    });     

  });

    
});