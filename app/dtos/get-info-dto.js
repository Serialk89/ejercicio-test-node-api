const { BaseDTO, fields } = require('dtox');

const GET_INFO_MAPPING = {
  id: fields.number(),
};
module.exports = class GetInfoDTO extends BaseDTO {
  constructor(data) {
    super(data, GET_INFO_MAPPING);
    // this.id = data.id;
  }
};
