const GetInfoDTO = require("./get-info-dto");
const { BaseDTO, fields } = require('dtox');
describe("GetInfoDTO DTO", () => {
       
  let getInfoDTO = null;

  beforeEach( () => {    
                  
  });

  afterEach( () => {
    getInfoDTO = null;
  });
     
  it("should be an instance of getInfoDTO ", () => {    
    getInfoDTO = new GetInfoDTO({id:15});
    expect(getInfoDTO instanceof GetInfoDTO).toBe(true);    
  });

  it("should be an instance of BaseDTO ", () => {    
    getInfoDTO = new GetInfoDTO({id:12});
    expect(getInfoDTO instanceof BaseDTO).toBe(true);    
  });

  it("should be 1 attributes", () => {                      
    getInfoDTO = new GetInfoDTO({id:12});
    let count = 0;
    for(value in getInfoDTO) {
      count++
    }
    expect(count).toBe(2);        
  });

  it("Throw error if data is null", () => {                      
    try{                                   
        getInfoDTO = new GetInfoDTO({});
        console.log('getInfoDTO; ',getInfoDTO);
      } catch (err) {
        //console.info(err);
        expect(err.message).toBe('Required property "id" is missing');
      }
  });

  it("Throw error if data is null", () => {                      
    try{                                   
        getInfoDTO = new GetInfoDTO();
        console.log('getInfoDTO; ',getInfoDTO);
      } catch (err) {
        //console.info(err);
        expect(err.message).toBe("Cannot read property 'id' of undefined");
      }
  });

  describe("attribute id", () => {

    it("should throw an excption", () => {
      try{                                   
        getInfoDTO = new GetInfoDTO({id:"sasa"});
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");               
        expect(err.message).toBe("Property of type number required");               
      }  
    });

    it("should return the correct value", () => {
      try{                                   
        getInfoDTO = new GetInfoDTO({id:1221});
        expect(getInfoDTO.id).toBe(1221);               
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }  
    });     

  });  
});
