var mockery = require('mockery');
var httpMocks = require('node-mocks-http');
var EventEmitter = require('events').EventEmitter;
var sinon = require("sinon");

describe('Security Middleware', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../config/index', {
      basePath: "any",
      versionNumber: "other"
    });

    mockery.registerMock('../loggers/logger', {
      info: () => {}, 
      error: () => {}, 
      debug: () => {}
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  it('When an valid token is passed, should run the next middleware', (done) => {
    // mock de la configuracion
    const token = "123";
    mockery.registerMock("../config/index", {
      accessToken: token
    });

    // mock del request
    var request = httpMocks.createRequest({
      url: "/", 
      headers: {
        "x-access-session": "session", 
        "x-access-token": token
      }, 
      body: {rut: "11.222.333-4"}
    });

    // mock del response
    var response = httpMocks.createResponse( {
      eventEmitter: EventEmitter
    });

    // prueba del middleware
    let flag = false;
    securityMiddleware = require("./security-middleware");
    securityMiddleware(request, response, () => {
      flag = true;
    });

    expect(flag).toBe(true);
    expect(response.statusCode).toBe(200);
    done();
  });

  it("When a invalid token is passed, an erroneous response should be generated", (done) => {
    // mock de la configuracion
    const accessToken = "123";
    mockery.registerMock("../config/index", {
      accessToken: accessToken
    });

    // mock del request
    const headerToken = "456";
    var request = httpMocks.createRequest({
      url: "/", 
      headers: {
        "x-access-session": "session", 
        "x-access-token": headerToken
      }, 
      body: {rut: "11.222.333-4"}
    });

    // mock del response
    var response = httpMocks.createResponse( {
      eventEmitter: EventEmitter
    });

    // prueba del middleware
    let flag = false;
    securityMiddleware = require("./security-middleware");
    securityMiddleware(request, response, () => {
      flag = true;
    });

    expect(flag).toBe(false);
    expect(response).toBeDefined();
    expect(response.statusCode).toBe(401);
    
    done();
  });
});
