const InfoMongoService = require('../services/persistence/info-mongo-service');

module.exports = class GetInfoController {
  getInfo(info) {
    const infoMongoService = new InfoMongoService();
    return new Promise((resolve, reject) => {
      infoMongoService.getInfo(info)
      .then((doc) => {
        resolve(doc);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }
};
