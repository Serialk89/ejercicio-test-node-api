var mockery = require('mockery');
var httpMocks = require('node-mocks-http');
var EventEmitter = require("events").EventEmitter;

describe('saveInfoPost route', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('../../loggers/logger', {
      debug: function () {}, 
      error: function() {}, 
      info: function() {}
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('Test Handler Function', () => {
    it('should not have errors', () => {
      // mock del controller
      class SaveInfoControllerMock {
        saveInfo(saveInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/save-info-controller", SaveInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        query: {
          "id": 1,
          "name": "Lala",
          "lastName": "Lolo",
          "email": "mail@gmail.com",
          "test": "11.111.111-1"
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./save-info-post-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
    });

    it('should return error when the body is incorrect', () => {
      // mock del controller
      class SaveInfoControllerMock {
        saveInfo(saveInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/save-info-controller", SaveInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        query: {}
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./save-info-post-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(403);
      expect(response._getData().message).toBe("Invalid parameters");
  });

  it("should return an error when something is wrong with mongo", () => {
      // mock de la clase new SaveInfoDTO(request.body);
      
      class SaveInfoDTOMock {
        constructor(body) {
          throw "Body error";
        }
      }

      mockery.registerMock('../dtos/save-info-dto', SaveInfoDTOMock);
      
      // mock del controller
      class SaveInfoControllerMock {
        saveInfo(saveInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/save-info-controller", SaveInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        query: {
          "id": 1,
          "name": "Lala",
          "lastName": "Lolo",
          "email": "mail@gmail.com",
          "test": "11.111.111-1"
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./save-info-post-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(403);
    });

    it("should return an error when the controller fails", () => {
      // mock del controller
      class SaveInfoControllerMock {
        saveInfo(saveInfoDTO) {
          return new Promise((resolve, reject) => {
            reject({});
          });
        }
      }

      mockery.registerMock("../controllers/save-info-controller", SaveInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        query: {
          "id": 1,
          "name": "Lala",
          "lastName": "Lolo",
          "email": "mail@gmail.com",
          "test": "11.111.111-1"
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./save-info-post-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
      //expect(response._getData().message).toBe("message");
    });
  });
});