const express = require('express');
const helper = require('../common/commonHelper');
const logger = require('../loggers/logger');
const SaveInfoDTO = require('../dtos/save-info-dto');
const SaveInfoController = require('../controllers/save-info-controller');

const router = express.Router();

function handler(request, response) {
  let saveInfoDTO;
  let toReturn;
  /* hice este "ajuste" debido a que al momento de usar postman
  siempre me tira un error por la variable id, lo enviaba como string y no como int */
  const cleanData = {
    id: parseInt(request.query.id, 10),
    name: request.query.name,
    lastName: request.query.lastName,
    email: request.query.email,
    test: request.query.test,
  };

  try {
    saveInfoDTO = new SaveInfoDTO(cleanData);
  } catch (err) {
    logger.debug('access not allowed, body: %s', JSON.stringify(request.query));
    helper.setResponseWithError(response, 403, 'Invalid parameters');
  }
  const saveInfoController = new SaveInfoController();
  saveInfoController.saveInfo(saveInfoDTO)
  .then((result) => {
    toReturn = helper.setResponse(response, 200, result);
  }).catch((error) => {
    toReturn = helper.setResponse(response, 403, error);
  });
  return toReturn;
}

router.post('/save-info', handler);

module.exports = router;
module.exports.handler = handler;
