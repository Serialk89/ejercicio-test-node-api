var mockery = require('mockery');
var httpMocks = require('node-mocks-http');
var EventEmitter = require("events").EventEmitter;

// suite -> is a group of test cases that can by used to test a specific behavior of the Javascript code.
describe('getInfo route', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('../../loggers/logger', {
      debug: function () {}, 
      error: function() {}, 
      info: function() {}
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('Test handler function', function(){
    it('Should not have error', function(){
   	  // mock del controller
      class GetInfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok"});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);
	  // mock del request 
      let request = httpMocks.createRequest({
        url: "/info/:id", 
        params: {
          "id": 15,
        }
      });
      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });
      // invocar a la ruta
      route = require("./get-info-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
      // expect(response._getData().message).toBe(200);
    });

    it('should return error when the query id is empty', () => {
      // mock del controller
      class GetInfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok"});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/info/:id", 
        params: {
        	id: ''
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./get-info-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(403);
      expect(response._getData().message).toBe("Invalid parameters");
    });

    it('should return error when the query is empty', () => {
      // mock del controller
      class GetInfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok"});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/info/:id", 
        params: {}
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./get-info-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(403);
      expect(response._getData().message).toBe("Invalid parameters");
    });

    it("should return an error when the controller fails", () => {
      // mock del controller
      class GetInfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            reject({});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        params: {
          "id": 15
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./get-info-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
      //expect(response._getData().message).toBe("message");
    });

    it("should return an error when something is wrong with mongo", () => {
      // mock de la clase new SaveInfoDTO(request.body);
      
      class GetInfoDTOMock {
        constructor(body) {
          throw "Body error";
        }
      }

      mockery.registerMock('../dtos/get-info-dto', GetInfoDTOMock);
      
      // mock del controller
      class GetInfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok"});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        params: {
          "id": 15
      	}
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./get-info-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(403);
    });
  });
});