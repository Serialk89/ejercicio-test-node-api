const express = require('express');
const helper = require('../common/commonHelper');
const GetInfoDTO = require('../dtos/get-info-dto');
const GetInfoController = require('../controllers/get-info-controller');

const router = express.Router();

function handler(request, response) {
  let getInfoDTO;
  let toReturn;
  let getInfoController;
  try {
    // force int
    if (Object.keys(request.params).length === 0 && request.params.constructor === Object) throw new Error('error');
    if (request.params.id === '') throw new Error('error');
    const data = { id: parseInt(request.params.id, 10) };
    getInfoDTO = new GetInfoDTO(data);
    getInfoController = new GetInfoController();
    getInfoController.getInfo(getInfoDTO)
    .then((result) => {
      // console.log('result: ', result);
      // if(result == null) throw 'No data.';
      toReturn = helper.setResponse(response, result);
    }).catch((error) => {
      toReturn = helper.setResponse(response, 403, error);
    });
  } catch (err) {
    // logger.debug('access not allowed, body: %s', JSON.stringify(request.query));
    toReturn = helper.setResponseWithError(response, 403, 'Invalid parameters');
  }
  return toReturn;
}

router.get('/:id', handler);

module.exports = router;
module.exports.handler = handler;
